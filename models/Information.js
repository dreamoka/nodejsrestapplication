const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema for Course
var referenceSchema = new Schema({
    data : String,
    created : {type: Date, default: Date.now}
});

var Information = new Schema({
    key : String,
    value : [referenceSchema]
},
{
    collection: 'records'
});

module.exports = mongoose.model('Information', Information);