let mongoose = require("mongoose");
let Information = require('../models/Information');

//Require the dev-dependencies
const server = require('../server');
const request = require('supertest');
const chai = require('chai');
const expect = chai.expect;

describe('Information', () => {
    beforeEach((done) => { //Before each test we empty the database
        Information.remove({}, (err) => { 
           done();         
        });     
    });
	
	describe('/GET Information', () => {
      it('it should not able to get any information', (done) => {
        request(server).get('/object/Test').end(function(err, res){
			expect(res.statusCode).to.equal(200);
			expect(res.body.result).to.equal('unable to find record');
			done(); 			
			});
	   });
     });
	 
	 
	  describe('# Create Information ', function() { 
	    let task = {'testing_key' : 'testing_value'};
		it('should create a information', function(done) { 
		  request(server).post('/object/') .send(task) .end(function(err, res) { 
			expect(res.statusCode).to.equal(200); 
			expect(res.body.key).to.equal('testing_key'); 
			expect(res.body.value).to.equal('testing_value'); 
			done(); 
		  }); 
		}); 
	  });
	  
	  describe('# Post Information Send Empty Json Object ', function() { 
	    let task = {};
		it('should not create a information', function(done) { 
		  request(server).post('/object/') .send(task) .end(function(err, res) { 
			expect(res.statusCode).to.equal(400); 
			expect(res.body.result).to.equal('Missing JSON Object in method post');
			done(); 
		  }); 
		}); 
	  });

	  describe('# Post Information Send More than 1 key ', function() { 
	    let task = {'testing_key' : 'testing_value', 'testing_key2': 'testing_value2'};
		it('should not create a information', function(done) { 
		  request(server).post('/object/') .send(task) .end(function(err, res) { 
			expect(res.statusCode).to.equal(400); 
			expect(res.body.result).to.equal('More than 1 Key');
			done(); 
		  }); 
		}); 
	  });
	  
	  describe('# Able to retrieve information ', function() { 
		it('it should able to retrievey information', (done) => {
			let information =  new Information({'key' : 'testing_key', 'value' : {'data' : 'testing_value'}});
			information.save((err, task) => {
				request(server).get('/object/testing_key').end(function(err, res){
					expect(res.statusCode).to.equal(200);
					expect(res.body.value).to.equal('testing_value');
					done(); 			
				});
			});
		}); 
	  });
	  
	  	describe('# Unable to retrieve information due to invalid timestamp letter ', function() { 
		it('Unable to retrieve information due to invalid timestamp letter', (done) => {
			let information =  new Information({'key' : 'testing_key', 'value' : {'data' : 'testing_value'}});
			information.save((err, task) => {
				request(server).get('/object/testing_key?timestamp=abcd').end(function(err, res){
					expect(res.statusCode).to.equal(400);
					expect(res.body.result).to.equal('Invalid timestamp');
					done(); 			
				});
			});
		}); 
	  });
	  
	  	describe('# Unable to retrieve information due to invalid timestamp negative number ', function() { 
		it('Unable to retrieve information due to invalid timestamp negative number', (done) => {
			let information =  new Information({'key' : 'testing_key', 'value' : {'data' : 'testing_value'}});
			information.save((err, task) => {
				request(server).get('/object/testing_key?timestamp=-1').end(function(err, res){
					expect(res.statusCode).to.equal(400);
					expect(res.body.result).to.equal('Invalid timestamp');
					done(); 			
				});
			});
		}); 
	  });
	  
	  	describe('# Unable to retrieve information due to invalid timestamp 0 ', function() { 
		it('Unable to retrieve information due to invalid timestamp 0', (done) => {
			let information =  new Information({'key' : 'testing_key', 'value' : {'data' : 'testing_value'}});
			information.save((err, task) => {
				request(server).get('/object/testing_key?timestamp=0').end(function(err, res){
					expect(res.statusCode).to.equal(400);
					expect(res.body.result).to.equal('Invalid timestamp');
					done(); 			
				});
			});
		}); 
	  });
	  
	  
	  describe('# Able to update information ', function() { 
		it('it should able to update any information', (done) => {
			let information =  new Information({'key' : 'testing_key', 'value' : {'data' : 'testing_value'}});
			information.save((err, record_info) => {
			  let task = {'testing_key' : 'testing_value123'};
			  request(server).post('/object/') .send(task) .end(function(err, res) { 
				expect(res.statusCode).to.equal(200); 
				expect(res.body.key).to.equal('testing_key'); 
				expect(res.body.value).to.equal('testing_value123'); 
				task = res.body; 
				done(); 
			  }); 
			});
		}); 
	  });
      	  
 });
