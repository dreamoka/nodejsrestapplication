 /**
 * Configurations of logger.
 */
var winston = require('winston');
const { createLogger, format, transports } = require('winston')
require('winston-daily-rotate-file');

var transport = new (transports.DailyRotateFile)({
    filename: 'log/application-%DATE%.log',
    datePattern: 'YYYY-MM-DD-HH',
	level:'error',
	json: false,
    zippedArchive: true,
    maxSize: '20m',
    maxFiles: '14d',
	colorize: true
});
  

var logger = createLogger({
	format: format.combine(
		format.timestamp(),
		format.json()
    ),
    transports: [
      transport
    ]
});

module.exports = logger;