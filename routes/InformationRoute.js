"use strict";

const moment = require('moment');
const express = require('express');
const app = express();
const router = express.Router();
const Information = require('../models/Information');
const logger = require('../util/logger');

router.route('/').post(function (req, res, next) {
	
	// Too much POST data, kill the connection!
	// 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
	if (req.body.length > 1e6)
	{
		logger.error('Too much post request data in post method.');
		return res.status(400).json({"result" : "Too much post request data"});
	}
	
	//Checking for empty JSON Object
	if(req.body.constructor === Object && Object.keys(req.body).length === 0) {
		logger.error('Missing JSON Object in method post in post method.');
		return res.status(400).json({"result" : 'Missing JSON Object in method post'});
	}
	
	let keys = Object.keys(req.body);
	if(keys.length > 1) {
		logger.error('More than 1 Key in post method.');
		return res.status(400).json({"result" : 'More than 1 Key'});
	}
	
	let keyvalue = keys[0];
	let valuefromreq = req.body[keyvalue];
	let currentdate = new Date(); 
	let jsonreturnvalue = { key: keyvalue.toLowerCase(), value: valuefromreq.toLowerCase(), timestamp : moment(currentdate).unix()};
	
	let query  = Information.where({ key: keyvalue.toLowerCase() });
	query.findOne(function (err, record) {
		if (err) return handleError(err);
		if (record) {
			//console.log(JSON.stringify(record, null, 2));
			Information.findByIdAndUpdate(
				record["_id"],
				{
					"$push": {
							   "value": {
								  $each: [ {data : valuefromreq.toLowerCase(), created : currentdate}],
								  $sort: { created: -1 }
								}
							}
				},
				{ "new": true },
				function (err, partner) {
					if (err)
					{
						logger.error('Failed in find one query in post method.');
						return res.status(500).json({"result" : 'Internal Error'});
					}
					return res.status(200).json(jsonreturnvalue);
				}
			);
		}
		else
		{
			let myobj = { key: keyvalue.toLowerCase(), value: {data : valuefromreq.toLowerCase(), created : currentdate}};
			const record = new Information(myobj);
			record.save()
			  .then(record => {
					return res.status(200).json(jsonreturnvalue);
			  })
			  .catch(err => {
					logger.error('Failed in find one query in post method.');
					return res.status(500).json({"result" : 'Internal Error'});
			  });
		}
	});
});

router.get('/:id', function (req, res) {
	
	res.set({ 'content-type': 'application/json; charset=utf-8' })
	
	// Too much Get data, kill the connection!
	// 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
	if (req.body.length > 1e6)
	{
		logger.error('Too much get request data in get method.');
		return res.status(400).json({"result" : "Too much get request data"});
	}

	let keyvalue = req.params["id"].toLowerCase();
	let unix_timestamp = req.query["timestamp"];
	let datequery = null;
	if (unix_timestamp) {
		
		if (typeof unix_timestamp !== 'number') {
			try {
				unix_timestamp = parseInt(unix_timestamp, 10);
			}catch(e)
			{
				logger.error('Failed in parseint in post method.');
				return res.status(500).json({"result" : "Internal Error"});
			}
		}
		
		if (isNaN(unix_timestamp) || !Number.isInteger(unix_timestamp) || 0 >= unix_timestamp) {
			logger.error('Invalid unix timestamp in get method.');
			return res.status(400).json({"result" : "Invalid timestamp"});
		 }
		  
		// Create a new JavaScript Date object based on the timestamp
		// multiplied by 1000 so that the argument is in milliseconds, not seconds.
		datequery = new Date(unix_timestamp*1000);		
	}
	else{
		datequery = new Date();
	}
	
	var steps = [
	{
		$match: {
		  'key': keyvalue
		}
	},
	{ $unwind : "$value" },
	{
		$match: {
		  "value.created": { $lte: datequery }
		}
	},
	{'$sort': 
		{'value.created': -1}
	},
	{'$limit': 1},
	{'$project' : { '_id': 0, 'value.data' : 1 } }
	];
	Information.aggregate(steps, function( err, result ) {
		if (err)
		{
			logger.error('Failed in find one query in get method.');
			return res.status(500).json('Internal Error');
		}
		if (result && Object.keys(result).length > 0)
		{
			return res.status(200).json({"value": result[0].value["data"]});
		}
		else
		{
			return res.status(200).json({"result" : "unable to find record"});
		}
	} );
});

module.exports = router;