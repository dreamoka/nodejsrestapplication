"use strict";

const logger = require('./util/logger');
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const app = express();
const config = require('./config/db');
const Information = require('./models/Information');
const InformationRoute = require('./routes/InformationRoute');

const PORT = process.env.PORT || '3002';

app.use(bodyParser.json());

mongoose.connect(config.DB).then(
    () => {logger.info('Database is connected'); },
    err => { logger.error('Can not connect to the database' + err.message)
});

app.use(bodyParser.json());
app.use('/object', InformationRoute);

app.listen(PORT, function(){
	logger.info("Your node js server is running on PORT: " + PORT);
});

module.exports = app; // for testing